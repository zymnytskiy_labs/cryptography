-module(euclid).
-export([gcd/2, extended/2]).

gcd(A, 0) -> A;
gcd(A, B) -> gcd(B, A rem B).

extended(A, 0) -> {A, 1, 0};
extended(A, B) ->
	{D, X, Y} = extended(B, A rem B),
	io:fwrite("~s, ~s - ~s div ~s * ~s (~s)~n", [integer_to_list(I) || I <- [Y, X, A, B, Y, X - A div B * Y]]),
	{D, Y, X - A div B * Y}.

