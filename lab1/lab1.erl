-module(lab1).
-export([start/0]).

-import(euclid, [extended/2]).

normalize(A, N) when A rem N >= 0 -> A rem N;
normalize(A, N) -> A rem N + N.

start() ->
	{ok, [A_]} = io:fread("Enter a: ", "~d"),
	{ok, [B_]} = io:fread("Enter b: ", "~d"),
	A = max(A_, B_), B = min(A_, B_),
	{D, X, Y} = euclid:extended(A, B),
	io:fwrite("~s * ~s + ~s * ~s = ~s~n", [integer_to_list(I) || I <- [X, A, Y, B, D]]),
	io:fwrite("(~s * ~s) mod ~s = ~s~n", [integer_to_list(I) || I <- [A, X, B, normalize(A * X, B)]]),
	io:fwrite("(~s * ~s) mod ~s = ~s~n", [integer_to_list(I) || I <- [B, Y, A, normalize(B * Y, A)]]).

