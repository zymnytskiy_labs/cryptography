#!/bin/sh

MODULE=lab1
DIR=$(dirname $0)

(cd $DIR && erlc $MODULE.erl && erl -noshell -s $MODULE start -s init stop)

