const norm = (k, l) => {
  const a = k % l;
  return a >= 0 ? a : (a + l);
};

class Cipher {
  constructor(alphabet) {
    this.alphabet = alphabet;
    this._ignore = [];
  }

  ignore(char) {
    if (this._ignore.includes(char)) {
      return char;
    }

    return null;
  }

  applyChar(char, func) {
    return this.ignore(char) || this.alphabet.char(this.norm(func.call(this, this.alphabet.index(char))));
  }

  apply(msg, func) {
    if (typeof msg === 'string') {
      return msg.split('').map(s => this.applyChar(s, func)).join('');
    } else if (Array.isArray(msg)) {
      return msg.map(s => this.applyChar(s, func));
    } else if (Buffer.isBuffer(msg)) {
      if (this.alphabet.isString) {
        return this.apply(msg.toString(), func);
      } else {
        return this.apply(Array.from(msg), func);
      }
    }

    throw new TypeError('wrong msg type');
  }

  setIgnore(ignore) {
    this._ignore = ignore;
  }

  encode(msg) {
    return this.apply(msg, this.encodeSymbol);
  }

  decode(msg) {
    return this.apply(msg, this.decodeSymbol);
  }

  norm(x) {
    return norm(x, this.alphabet.length);
  }
}

module.exports = Cipher;
