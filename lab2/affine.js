const Cipher = require('./cipher');

const gcd = (a, b) => b === 0 ? a : gcd(b, a % b);

const extended = (a, b) => {
  if (b === 0) return [a, 1, 0];

  const [d, x, y] = extended(b, a % b);
  return [d, y, x - Math.floor(a / b) * y];
};

class AffineCipher extends Cipher {
  constructor(a, b, alphabet) {
    super(alphabet);

    this.a = a;
    this.b = b;

    console.log(a, b);
    const [d, x, y] = extended(alphabet.length, a);
    console.log(d, x, y);

    if (d !== 1) {
      throw new Error('bad keys');
    }

    this.y = this.norm(y);
  }

  encodeSymbol(code) {
    return code * this.a + this.b;
  }

  decodeSymbol(code) {
    return this.norm(code - this.b) * this.y;
  }
}

module.exports = AffineCipher;

