const fs = require('fs');
const affine = require('./affine');
const ceaser = require('./ceaser');

const alphabet = 'абвгґдеєжзиіїйклмнопрстуфхцчшщьюя .';
// const alphabet = 'АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюя ,.()-—/:\n\'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');

fs.readFile('./message.txt', (err, data) => {
  if (err) throw err;

  console.log(data.toString());
  console.log();

  {
    console.log(data.toString().toLowerCase().replace(/\n/g, ' '));
    const crypted = affine.crypt(alphabet, 3, 3, data.toString().toLowerCase().replace(/\n/g, ' '));
    const decrypted = affine.decrypt(alphabet, 3, 3, crypted);
    console.log(crypted);
    console.log(decrypted === data.toString());
    console.log();
  }

  {
    const crypted = ceaser.crypt(alphabet, 7, data.toString());
    const decrypted = ceaser.decrypt(alphabet, 7, crypted);
    console.log(crypted);
    console.log(decrypted === data.toString());
  }
});
