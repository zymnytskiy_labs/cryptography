const Cipher = require('./cipher');

class VigenereCipher extends Cipher {
  constructor(key, alphabet) {
    super(alphabet);

    this.key = key.split('').map(s => alphabet.index(s));
    this.count = 0;
  }

  encodeSymbol(code) {
    return code + this.key[this.count++ % this.key.length];
  }

  decodeSymbol(code) {
    return code - this.key[this.count++ % this.key.length];
  }
}

module.exports = VigenereCipher;

