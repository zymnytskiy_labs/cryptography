#!/usr/bin/env node

const path = require('path');

const CeaserCipher = require('./ceaser');
const AffineCipher = require('./affine');
const VigenereCipher = require('./vigenere');

const Alphabet = require('./alphabet');

const possibleAlgorithms = ['ceaser-crypt', 'ceaser-decrypt', 'affine-crypt', 'affine-decrypt', 'vigenere-crypt', 'vigenere-decrypt'];

const algorithm = path.basename(process.argv[1]);

if (!possibleAlgorithms.includes(algorithm)) {
  throw new Error(`Unknown algorithm ${algorithm}`);
}

let yargs = require('yargs')
  .usage('Usage: $0 -k <key>')
  .alias('a', 'alphabet')
  .string('a')
  .alias('i', 'ignore')
  .string('i')
  .choices('algorithm', possibleAlgorithms)
  .help()
  .alias('h', 'help')

if (algorithm.startsWith('affine-')) {
  yargs = yargs
    .alias('k', 'keys')
    .nargs('k', 2)
    .demandOption(['k']);
}

if (algorithm.startsWith('ceaser-') || algorithm.startsWith('vigenere-')) {
  yargs = yargs.alias('k', 'key')
  .demandOption(['k']);
}

const { argv } = yargs;

const specials = (x) => x.replace(/\\n/, '\n');

const alphabet = argv.alphabet ? new Alphabet(specials(argv.alphabet.toString())) : new Alphabet(Array.from({ length: 256 }, (v, k) => k));
const ignore = specials(argv.ignore || '');

const unique = (arr) => arr.filter((c, i) => arr.indexOf(c) === i);

const getCipher = () => {
  switch (algorithm) {
    case 'ceaser-crypt': return [new CeaserCipher(argv.key, alphabet), 'encode'];
    case 'ceaser-decrypt': return [new CeaserCipher(argv.key, alphabet), 'decode'];
    case 'affine-crypt': return [new AffineCipher(...argv.keys.map(k => parseInt(k)).sort(), alphabet), 'encode'];
    case 'affine-decrypt': return [new AffineCipher(...argv.keys.map(k => parseInt(k)).sort(), alphabet), 'decode'];
    case 'vigenere-crypt': return [new VigenereCipher(argv.key, alphabet), 'encode'];
    case 'vigenere-decrypt': return [new VigenereCipher(argv.key, alphabet), 'decode'];
    default: throw new Error('bad algorithm');
  }
}

const showWarning = (() => {
  let show = true;

  return () => {
    if (show) {
      process.stderr.write('WARN: some characters could be missed\n');
    }

    show = false;
  };
})();

const [cipher, mode] = getCipher();

cipher.setIgnore(ignore);

process.stdin.on('data', (data) => {
  process.stdout.write(mode === 'encode' ? cipher.encode(data) : cipher.decode(data));
});

