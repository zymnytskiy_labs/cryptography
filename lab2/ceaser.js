const Cipher = require('./cipher');

class CeaserCipher extends Cipher {
  constructor(key, alphabet) {
    super(alphabet);

    this.key = key;
  }

  encodeSymbol(code) {
    return code + this.key;
  }

  decodeSymbol(code) {
    return code - this.key;
  }
}

module.exports = CeaserCipher;

