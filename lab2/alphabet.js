class Alphabet {
  constructor(chars) {
    if (typeof chars === 'string') {
      this.chars = chars.split('');
      this.isString = true;
    } else if (Array.isArray(chars)) {
      this.chars = chars;
    } else {
      throw new Error('unknown type of alphabet');
    }
  }

  index(char) {
    const i = this.chars.indexOf(char);
    if (i === -1) throw new Error(`bad character '${char}'`);
    return i;
  }

  has(char) {
    return this.chars.includes(char);
  }

  char(index) {
    if (index < 0 || index >= this.chars.length) {
      throw new Error('bad index');
    }

    return this.chars[index];
  }

  get length() {
    return this.chars.length;
  }
}

module.exports = Alphabet;
