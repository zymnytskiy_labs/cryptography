const sort = (o) => Object.keys(o).sort((a, b) => {
  if (o[a] == o[b]) return 0;
  if (o[a] < o[b]) return 1;

  return -1;
}).reduce((prev, curr) => {
  prev[curr] = o[curr];
  return prev;
}, {});

const unique = (a) => a.filter((e, i) => a.indexOf(e) === i);
const count = (text, str) => {
  const matches = text.match(new RegExp(str, 'g'));
  return matches ? matches.length : 0;
};

const analyze = (text) => {
  const letters = text.replace(/\n/g, ' ').split('');
  const freqs = {};

  letters.forEach(c => {
    !freqs[c] && (freqs[c] = 0);
    freqs[c]++;
  });

  const bigrams = sort(unique(letters.map((l, i) => l + letters[i + 1])).filter(l => l.length === 2).reduce((prev, curr) => {
    prev[curr] = count(text.replace(/\n/g, ' '), curr);
    return prev;
  }, {}));

  return { letters: sort(freqs), bigrams };
};

let text = '';

process.stdin.on('data', data => { text += data; });
process.stdin.on('end', () => console.log(JSON.stringify(analyze(text), null, 4)));

