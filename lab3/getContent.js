const jsdom = require('jsdom');

let page = '';

function calcFreq(text) {
        var result = "абвгґдеєжзиіїйклмнопрстуфхцчшщьюя".split('').reduce(function(result, letter) {
                result[letter] = 0;
                return result;
        }, {});

        text.split('').forEach(function(letter) {
                result[letter]++;
        });

        return Object.keys(result).reduce(function(res, letter) {
                res[letter] = result[letter] / text.length;
                return res;
        }, {});
}

process.stdin.on('data', (data) => { page += data; });
process.stdin.on('end', () => {
  jsdom.env(page, (err, window) => {
    const text = Array.from(window.document.querySelectorAll('#mw-content-text p')).map(p => p.textContent).join('\n\n');
    console.error(calcFreq(text));
    console.log(text);
  });
});

